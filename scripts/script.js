

function afficherProposition(mot){	
	let divZoneProposition = document.querySelector('.zoneProposition')
	divZoneProposition.innerHTML = `${mot}`
	return divZoneProposition.innerHTML
}

function choixFonction(arg){
	let btnValider = document.getElementById('btnValiderMot')
	btnValider.setAttribute("onclick", arg)
	return btnValider
}


async function crypt() {
    let pyodide = await loadPyodide();
    result = pyodide.runPython(`
from js import afficherSaisie, message
from random import randint

def crypt_caract(caractere, decalage):
	"""Fonction qui crypte un caractère en appliquant un décalage spécifique."""
	code = chr(ord(caractere)+decalage)
	return code

def crypt(saisie):
	"""Fonction qui crypte une saisie à l'aide de la fonction qui crypte ses caractères."""
	decalage = randint(1,300)
	message_crypt =""
	for lettre in saisie:
		message_crypt += crypt_caract(lettre,decalage)
	cle_message_crypt = chr(decalage)+message_crypt
	return cle_message_crypt

message_crypte = crypt(message)
    `);

	let message_crypte = pyodide.globals.get('message_crypte');
	afficherProposition(message_crypte)
	console.log(message_crypte)
	afficherSaisie.value =""
}


async function decrypt() {
    let pyodide = await loadPyodide();
    result = pyodide.runPython(`
from js import afficherSaisie, message
from random import randint
def decrypt_caract(char, decalage):
	"""Fonction qui décrypte un caractère en appliquant un décalage spécifique."""
	actuel = ord(char)
	actuel -= decalage
	return chr(actuel)

def decrypt(inser):
	"""Fonction qui décrypte un saisie à l'aide de la fonction qui crypte ses caractères."""
	message_decrypt = ""
	message_code = inser[1:]
	decalage = ord(inser[0])
	for codage in message_code:
		message_decrypt += decrypt_caract(codage,decalage)
	return message_decrypt

message_decrypte = decrypt(message)
    `);
    let message_decrypte = pyodide.globals.get('message_decrypte');
	afficherProposition(message_decrypte)
	console.log(message_decrypte)
	afficherSaisie.value =""
}



// let btnValider = document.getElementById('btnValiderMot')



let option = document.querySelectorAll(".optionSource input")
for (let i = 0 ; i < option.length ; i++){
	option[i].addEventListener("change", (event) => {
		console.log(event.target.id)
		if (event.target.id === "crypter"){
			choixFonction("crypt()")
			console.log(choixFonction("crypt()") )

		} else {
			choixFonction("decrypt()")
			console.log(choixFonction("decrypt()"))
			
		}
	})
}

let btnValider = document.getElementById('btnValiderMot')
btnValider.addEventListener("click", function(){
	afficherSaisie = document.getElementById('inputEcriture')
	message = afficherSaisie.value	
})